import React, {Component} from "react";

class TodoInput extends Component {
    state = {newTodo: ''};

    changeText = evt => this.setState({newTodo: evt.target.value});

    handleAdd = () => {
        this.props.onSubmit(this.state.newTodo);
        this.setState({newTodo: ''})
    };

    render() {
        return (
            <div>
                <input onChange={this.changeText} value={this.state.newTodo} disabled={this.props.limit}/>
                <button onClick={this.handleAdd} disabled={this.props.limit}>Add</button>
            </div>
        );
    }
}

export default TodoInput;