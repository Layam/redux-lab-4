import React, {Component} from "react";

class TodoItem extends Component {

    up = () => {
        this.props.upTodo(this.props.todo)
    };

    render() {
        return (
            <div>
                <li style={this.props.todo.style}>{this.props.todo.text}</li>
                <button onClick={this.up}>Up</button>
            </div>
        );
    }

}
;

export default TodoItem;