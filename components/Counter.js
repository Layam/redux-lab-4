import React, { Component } from 'react';

class Counter extends Component {

    render() {
        return (
            <div>
                <span>{this.props.counter}</span>
            </div>
        );
    }
}

export default Counter;