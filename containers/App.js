import React, {Component} from "react";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import TodoItem from "../components/TodoItem";
import TodoInput from "../components/TodoInput";
import addTodo from "../actions/add_todo";
import upTodo from "../actions/up_todo";
import clearTodo from "../actions/clear_todo";
import filterTodo from "../actions/filter_todo";

class App extends Component {

    filterTodo = event => {
        this.props.filterTodo(event.target.value)
    };

    render() {
        return (
            <div>
                <h1>Todo List</h1>

                {
                    this.props.filteredTodos.map((todo) =>
                        <ul key={todo.text}>
                            <TodoItem todo={todo} upTodo={this.props.upTodo}/>
                        </ul>
                    )
                }

                <TodoInput onSubmit={this.props.addTodo}/>

                <br />

                <input onChange={this.filterTodo}/>
                <button onClick={this.props.clearTodo}>Clear</button>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    todos: state.todos.todos,
    filteredTodos: state.todos.filteredTodos
});

const mapDispatchToProps = dispatch => bindActionCreators(
    {
        addTodo,
        upTodo,
        clearTodo,
        filterTodo
    },
    dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(App);