export default todo => dispatch => {
    dispatch({
        type: 'UP_TODO',
        todo
    });
};