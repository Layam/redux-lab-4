export default query => dispatch => {
	dispatch ({
		type: 'FILTER_TODO',
		query
	});
};