export default text => dispatch => {
    dispatch({
        type: 'ADD_TODO',
        text
    });
};