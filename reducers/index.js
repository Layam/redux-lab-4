const initialState = {
    todos: [],
    filteredTodos: [],
    query: ''
};

const filterByText = (state, action) => {
    return {
        todos: state.todos,
        filteredTodos: state.todos.filter((value) => {
            return value.text.indexOf(state.query) > -1;
        }),
        query: state.query
    }
};

const todosReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'ADD_TODO':
            state.todos = state.todos.concat({
                text: action.text
            });
            return filterByText(state, action);
        case 'UP_TODO':
            let index = _.findIndex(state.todos, item => item.text == action.todo.text);
            action.todo.style = {color: 'red'};
            state.todos.splice(0, 0, action.todo);
            state.todos.splice(index + 1, 1);
            return filterByText(state, action);
        case 'CLEAR_TODO':
            return {
                todos: [],
                filteredTodos: [],
                query: ''
            };
        case 'FILTER_TODO':
            state.query = action.query || '';
            return filterByText(state, action);
        default:
            return state;
    }
}

export default {todos: todosReducer};